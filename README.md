# Capstone Resources

Resources designed to assist students in the CS Capstone experience.

There is a fairly comprehensive list of technologies located at
https://github.com/sindresorhus/awesome. This repository will serve to expand
the GitHub list with useful peer-curated links.

## Contents
- [Tutorials](#tutorials)-

## Tutorials
- [w3schools](https://www.w3schools.com/) - Useful web development resource for
  HTML, CSS, and Javascript
- [Microsoft Learn](https://docs.microsoft.com/en-us/learn/) - Teaching
  Microsoft products.
- [pygame](https://www.pygame.org/wiki/GettingStarted) - Introduction to pygame
- [udacity](https://www.udacity.com/) - free/paid courses on various computer
  science topics.
- [code4startup](https://code4startup.com/) - teaches the entire process of
  creating a project
- [codeacademy](https://www.codecademy.com/) - beginner friendly tutorials on
  a variety of languages / platforms.
- [learnxinyminutes](https://learnxinyminutes.com/) - quick tutorial into a
  large amount of languages
- [theodinproject](https://www.theodinproject.com/) - open source full stack web
  development curriculum
- [learnpython](https://www.learnpython.org/) - free python tutorial
- [thenewboston](https://www.youtube.com/user/thenewboston) - video tutorials on
    modern technology / languages
